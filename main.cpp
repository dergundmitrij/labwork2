#include <iostream>
#include <regex>
#include <fstream>
#include <vector>

size_t CountingBytes(const std::string filename) {
    std::ifstream fin;
    fin.open(filename, std::ios::in | std::ifstream::binary | std::ifstream::ate);
    if (!fin.is_open()) {
        std::cout << "File cannot be opened!" << std::endl;
        exit(1);
    }

    return fin.tellg();
}
size_t CountingLines(const std::string filename) {
    std::ifstream fin;
    fin.open(filename, std::ios::in);
    int counter = 0;

    if (!fin.is_open()) {
        std::cout << "File cannot be opened!" << std::endl;
        exit(1);
    } else {
        std::string str;
        while (std::getline(fin,str)) {
            counter++;
        }
    }
    return counter;
}
size_t CountingWords(const std::string filename) {
    std::ifstream fin;
    fin.open(filename, std::ios::in);
    if (!fin.is_open()) {
        std::cout << "File cannot be opened!" << std::endl;
        exit(1);
    }

    std::string str;
    std::string buff;
    while (std::getline(fin, buff)) {
        str += buff + " ";
    }

    std::regex reg("\\S+");
    //Объявляем итератор, который будет обходить строку и последовательно возвращать слова из этой строки
    auto start = std::sregex_iterator(str.begin(), str.end(), reg);
    //Объявляем пустой итератор для сравнения в distance
    auto finish = std::sregex_iterator();

    //Возвращаем количество итераций start, необходимое для совпадения с finish
    return std::distance(start, finish);
}

size_t AvgLetters(const std::string filename){

    std::vector<int> lettersCount;

    std::ifstream fin;
    fin.open(filename, std::ios::in);
    if (!fin.is_open()) {
        std::cout << "File cannot be opened!" << std::endl;
        exit(1);
    }

    std::string str;
    std::string buff;
    while (std::getline(fin, buff)) {
        str += buff + " ";
    }

    int lastSpace = -1;

    for(int i = 0; i < str.length(); i++){
        if(str[i] == ' '){
            if(i - lastSpace - 1 > 0) lettersCount.push_back(i - lastSpace - 1);
            lastSpace = i;
        }
    }

    int sum = 0;
    for (int i = 0; i < lettersCount.size(); i++) {
        sum += lettersCount[i];
    }

    return sum / lettersCount.size();
}

struct Flags {
    bool dispBytes = false;
    bool dispLines = false;
    bool dispWords = false;
    bool dispAvgLetters = false;
};

int main(int argc, char *argv[]) {

    Flags flags;

    std::string filename;

    if (argc < 2) {
        std::cout << "You didn't provide any parameter!" << std::endl;
        exit(1);
    }

    //Начинаем с i = 1, так как параметром под индексом 0 всегда будет название скрипта
    for (int i = 1; i < argc; i++) {
        std::string arg = argv[i];
        //Проверяем, является ли элемент параметром или названием файла
        if (arg.length() < 2) {
            std::cout << "Incorrect parameter" << std::endl;
            exit(1);
        }
        if (arg[0] == '-') {
            if (arg[1] == '-') {
                if(arg == "--bytes") flags.dispBytes = true;
                if(arg == "--lines") flags.dispLines = true;
                if(arg == "--words") flags.dispWords = true;
                if(arg == "--average") flags.dispAvgLetters = true;
            } else {
                if (regex_match(arg,std::regex ("[-]{1}[alwc]{1,4}"))) {
                    if(arg.find("c") != -1) flags.dispBytes = true;
                    if(arg.find('l') != -1) flags.dispLines = true;
                    if(arg.find('w') != -1) flags.dispWords = true;
                    if(arg.find('a') != -1) flags.dispAvgLetters = true;
                } else {
                    std::cout << "Incorrect parameter" << std::endl;
                    exit(1);
                }
            }
        } else {
            //Название файла должно соответствовать шаблону <название>.<расширение>
            std::regex reg("[a-zA-Z0-9-_]+\\.{1}[a-z]+");
            if (std::regex_match(arg, reg)) {
                filename = arg;
            }
        }
    }

    if (argc == 2) {
        std::cout << CountingLines(filename) << " " << CountingWords(filename) << " " << CountingBytes(filename) << " " << filename << std::endl;
    } else {
        if (flags.dispLines) {
            std::cout << CountingLines(filename) << " ";
        }
        if (flags.dispWords) {
            std::cout << CountingWords(filename) << " ";
        }
        if (flags.dispBytes) {
            std::cout << CountingBytes(filename) << " ";
        }
        if (flags.dispAvgLetters) {
            std::cout << CountingBytes(filename) << " ";
        }
        std::cout << filename << std::endl;
    }

    return 0;
}

